import React, { useEffect } from "react";
import { withSkeletonLoader } from "./SkeletonLoader";
import '../styles/card.scss';

function GenericCard2(props) {
  const { setLoading } = props;

  useEffect(() => {
    setTimeout(() => setLoading(false), 3000);
  }, [setLoading]);
  return (
    <div className="card">
    <div className="card__header">
      <img className="card__header__media" src="https://lorempixel.com/60/60/abstract" alt="title" />
      <div className="card__header__title-section">
        <div className="card__header__title">Generic Card 2</div>
        <div className="card__header__subtitle">some subtitle</div>
      </div>
      </div>
      <div className="card__body">
        <div className="card__body__text">{`The linear-gradient() CSS function creates an image consisting of a progressive transition between two or more colors along a straight line. Its result is an object of the <gradient> data type, which is a special kind of <image>.`}</div>
      </div>
    </div>
  );
}

const skeletonConfig = {
  height: "200px",
  width: "500px",
  skeletonStyles: {
    borderRadius: "6px",
  },
  hasHeader: true,
  hasHeaderWithMedia: true,
  hasBody: true,
  wrapperClassName: 'card',
};

export default withSkeletonLoader(GenericCard2, skeletonConfig);
