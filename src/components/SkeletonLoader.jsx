import React, { useState } from "react";
import PropTypes from "prop-types";
import Skeleton from "./Skeleton";


function SkeletonLoader(props) {
  const { skeleton, children, loading = false } = props;

  return (
    <div className="skeleton-loader">
      {loading && skeleton}
      <span
        style={{
          display: loading ? "none" : null,
        }}
      >
        {children}
      </span>
    </div>
  );
}

SkeletonLoader.propTypes = {
  skeleton: PropTypes.element.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.node,
    PropTypes.element,
  ]).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default SkeletonLoader;


export function withSkeletonLoader(ActualComponent, skeletonProps = {}) {
  const skeleton = <Skeleton {...skeletonProps} />;

  return (props) => {
    const [loading, setLoading] = useState(true);

    return (
      <SkeletonLoader loading={loading} skeleton={skeleton}>
        <ActualComponent {...props} setLoading={setLoading} />
      </SkeletonLoader>
    );
  };
}
