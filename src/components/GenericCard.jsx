import React, { useEffect } from "react";
import { withSkeletonLoader } from "./SkeletonLoader";
import '../styles/card.scss';

function GenericCard(props) {
  const { setLoading } = props;

  useEffect(() => {
    setTimeout(() => setLoading(false), 3000);
  }, [setLoading]);

  return (
    <div className="card card--generic">
    <div className="card__header">
      <div className="card__header__title-section">
        <div className="card__header__title">Generic Card</div>
        <div className="card__header__subtitle">some subtitle</div>
      </div>
      </div>
      <div className="card__body">
        <img
          className="card__body__media"
          src="https://lorempixel.com/400/200/abstract"
          alt="random"
        />
        <div className="card__body__text">{`The linear-gradient() CSS function creates an image consisting of a progressive transition between two or more colors along a straight line. Its result is an object of the <gradient> data type, which is a special kind of <image>.`}</div>
      </div>
    </div>
  );
}

const skeletonConfig = {
  height: "300px",
  width: "500px",
  skeletonStyles: {
    borderRadius: "6px",
  },
  hasHeader: true,
  hasBody: true,
  hasBodyWithMedia: true,
  wrapperClassName: 'card',
};

export default withSkeletonLoader(GenericCard, skeletonConfig);
