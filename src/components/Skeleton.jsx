import React from "react";
import PropTypes from "prop-types";

import "../styles/skeleton.scss";

 function Skeleton(props) {
  const {
    hasHeader,
    hasHeaderWithMedia,
    hasBody,
    hasBodyWithMedia,
    height = "100%",
    width = "100%",
    skeletonStyles = {},
    wrapperClassName = "",
    bodyRowCount = 5,
    bodyRowClassName = "",
  } = props;

  const bodyRows = hasBody ? new Array(bodyRowCount).fill(0).map((_, index) => (
    <div
      key={index}
      className={`skeleton__body__paragraph-text
        ${bodyRowClassName} ${
        index === bodyRowCount - 1 ? "skeleton__body__paragraph-text--half" : ""
      }`}
    ></div>
  )) : [];

  return (
    <div
      className={`skeleton ${wrapperClassName}`}
      style={{
        height,
        width,
        ...skeletonStyles,
      }}
    >
      {hasHeader && (
        <div className="skeleton__header">
          {hasHeaderWithMedia && (
            <div className="skeleton__header__media"></div>
          )}
          <div className="skeleton__header__title-section">
            <div className="skeleton__header__title"></div>
            <div className="skeleton__header__subtitle"></div>
          </div>
        </div>
      )}
      {hasBody && (
        <div className="skeleton__body">
          {hasBodyWithMedia && <div className="skeleton__body__media"></div>}
          {bodyRows}
        </div>
      )}
    </div>
  );
}

Skeleton.propTypes = {
  hasHeader: PropTypes.bool,
  hasHeaderWithMedia: PropTypes.bool,
  hasBody: PropTypes.bool,
  hasBodyWithMedia: PropTypes.bool,
  height: PropTypes.string,
  width: PropTypes.string,
  skeletonStyles: PropTypes.object,
  wrapperClassName: PropTypes.string,
  bodyRowCount: PropTypes.number,
  bodyRowClassName: PropTypes.string,
}


export default Skeleton;
