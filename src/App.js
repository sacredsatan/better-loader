import './App.scss';
import GenericCard from './components/GenericCard';
import GenericCard2 from './components/GenericCard2';

function App() {
  return (
    <div className="App">
      <GenericCard />
      <GenericCard2 />
    </div>
  );
}

export default App;
