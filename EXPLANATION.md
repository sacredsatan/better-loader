# A better loading experience

This pattern is familiar to me with the name Skeleton Loader, for this task I've implemented a few things:
1. `SkeletonLoader` component, which looks similar to `React.Suspense` (it takes `children` to render, a `skeleton` to rednder while it's loading, and a `loading` prop that'll state if it is loading or not)
2. `Skeleton` component which takes some config as props and returns a generic _skeleton-like_ layout
3. `withSkeletonLoader` higher order component for convenience

## Reasonings

#### "SkeletonLoader" component
This is a pretty simple component, as soon as I read the problem, it just made sense to make it like a generic wrapper component, which renders one thing or the other based on props passed to it.

##### Props

| Prop Name |    Type    | Description                  | DefaultValue | Optional |
|-----------|:----------:|------------------------------|--------------|----------|
| skeleton  | Renderable | The loader component         | NA           | false    |
| children  | Renderable | The actual component to load | NA           | false    |
| loading   | bool       | loading state                | false        | false    |

#### "Skeleton" component
`Skeleton` component takes some config, and returns a _skeleton-like_ layout. There's really no limit to what you can allow to be customizable in a componet like this. This is still pretty opinionated, but should cover most card like use cases. It's also possible to just skip using this component altogether and use `SkeletonLoader` alone.

##### Props

| Prop Name          |  Type  | Description                                  | DefaultValue | Optional |
|--------------------|:------:|----------------------------------------------|--------------|----------|
| hasHeader          | bool   | If the loader should have header             | false        | true     |
| hasHeaderWithMedia | bool   | If the loader should have media in header    | false        | true     |
| hasBody            | bool   | If the loader should have body               | false        | true     |
| hasBodyWithMedia   | bool   | If the loader should have media in body      | false        | true     |
| height             | string | `height` css property value for main wrapper | "100%"       | true     |
| width              | string | `width` css property value for main wrapper  | "100%"       | true     |
| skeletonStyles`    | object | valid css style object for main wrapper      | {}           | true     |
| wrapperClassName   | strign | classname for main wrapper                   | ""           | true     |
| bodyRowCount       | number | number of rows to show in skeleton body      | 5            | true     |
| bodyRowClassName   | string | classname for each row rendered in skeleton  | ""           | true     |

#### "withSkeletonLoader" function

This function returns a highter order component that passes control of showing and hiding the loader to the child being rendered. This is pretty useful in cases where components are responsible for fetching their own data. The function takes 2 arguments, the actual component to wrap and `skeletonConfig` to create a skeleton.

It passes down `setLoading` function to the child component, where it can set it to `false` whenever loading is finished.


## How to Run
This project was created with CRA, so it's just a matter of installing dependencies (`yarn`, `npm install`) and startig the dev server (`yarn start`, `npm start`).


## Live Demo
This project's live demo is hosted at https://sacredsatan.gitlab.io/better-loader/
